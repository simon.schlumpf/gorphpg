# GorphPG: Go & PostgreSQL based super minimal Graph Database imitation for the poor

GorphPG is a lightweight, minimalistic graph database imitation built using Go and PostgreSQL. It's designed for simple applications that need graph-like data structures but don't require the full power or complexity of a dedicated graph database.

## Features
- **Node Management:** Easily create, update, and delete nodes.
- **Edge Management:** Efficiently manage relationships between nodes.
- **Flexible Data Types:** Store a wide range of data types within nodes and edges.
- **Search and Query:** Retrieve nodes and edges based on various criteria.

## Getting Started
### Prerequisites
- Go (latest version recommended)
- PostgreSQL

### Installation
1. Clone the repository.
2. Set up the PostgreSQL database.
3. Run `go build` to compile the project.

### Usage
- Initialize the `GorphPG` struct with a graph name, database pool and logger.
- Use the provided functions to interact with nodes and edges in your database.

## Code Structure
- **GorphPG Struct:** Central structure managing database connections and logging.
- **Node and Edge Types:** Structures representing graph nodes and edges.

## API Reference
- `CreateNode()`: Create a new node.
- `UpdateNode()`: Update an existing node.
- `DeleteNode()`: Delete a node.
- `CreateEdge()`: Create a relationship between two nodes.
- `UpdateEdge()`: Update an existing edge.
- `DeleteEdge()`: Delete an edge.
- More functions for node and edge retrieval and manipulation.

## ToDos
- [x] **Node Lookup:** Retrieve a node or set of nodes based on criteria like ID, label, or property values.
- [x] **Relationship Lookup:** Find relationships between nodes, often based on relationship type or properties.
- [x] **Creating Nodes:** Add new nodes to the graph with specific properties and labels.
- [x] **Creating Edges:** Establish new relationships between nodes, with properties and types.
- [ ] **Deleting Nodes:** Remove nodes from the graph, often along with their associated relationships.
- [ ] **Deleting Edges:** Remove specific relationships between nodes in the graph.
- [ ] **Recursive Queries:** Perform recursive traversals of relationships, like finding all descendants in a family tree.
- [ ] **Pathfinding Queries:** Identify the shortest path or all paths between two nodes.
- [ ] **Pattern Matching:** Look for specific patterns or shapes within the graph, such as interconnected nodes.
- [ ] **Aggregation Queries:** Aggregate data across nodes and relationships, like counting nodes with a certain property.
- [ ] **Graph Traversal:** Traverse the graph, starting from nodes and following relationships to other nodes.
- [ ] **Centrality Analysis:** Identify important nodes based on their position in the network.
- [ ] **Community Detection:** Identify clusters or communities within the graph.
- [ ] **Constraint Queries:** Enforce data integrity, such as ensuring unique properties across nodes of a certain type.

