package GorphPG

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"github.com/rs/zerolog"

	"github.com/lib/pq"
)

var Logger zerolog.Logger

type GorphPG struct {
	Name   string
	Pool   *pgxpool.Pool
	Logger zerolog.Logger
}

type Node struct {
	ID    uuid.UUID   // Unique identifier
	Type  string      // Type of the node
	Value interface{} // Interface to store any struct as value
}

type Edge struct {
	ID     uuid.UUID // Unique identifier for the edge
	Source uuid.UUID // UUID of the source node
	Target uuid.UUID // UUID of the target node
	Type   string    // Type of the edge (optional)
}

func NewGraph(name string, pool *pgxpool.Pool, logger zerolog.Logger) (*GorphPG, error) {
	// Regular expression to validate the graph name
	validName := regexp.MustCompile(`^[a-zA-Z0-9_]+$`)
	if !validName.MatchString(name) {
		return nil, errors.New("invalid graph name; only alphanumeric characters and underscores are allowed")
	}

	return &GorphPG{
		Name:   name,
		Pool:   pool,
		Logger: logger,
	}, nil
}

// InitializeDatabase sets up the necessary tables and structures in the database.
func (g *GorphPG) InitializeDatabase() error {
	ctx := context.Background()

	// Create the UUID extension
	_, err := g.Pool.Exec(ctx, "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")
	if err != nil {
		return err
	}

	// Create the nodes table with prefix
	nodesTable := fmt.Sprintf("%s_nodes", g.Name)
	_, err = g.Pool.Exec(ctx, fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s (
            node_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
            node_type VARCHAR(50),
            node_value JSONB
        );
    `, pq.QuoteIdentifier(nodesTable)))
	if err != nil {
		return err
	}

	// Create the edges table with prefix
	edgesTable := fmt.Sprintf("%s_edges", g.Name)
	_, err = g.Pool.Exec(ctx, fmt.Sprintf(`
		 CREATE TABLE IF NOT EXISTS %s (
			 edge_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
			 source_node_id UUID REFERENCES %s(node_id),
			 target_node_id UUID REFERENCES %s(node_id),
			 edge_type VARCHAR(100)
		 );
	 `, pq.QuoteIdentifier(edgesTable), pq.QuoteIdentifier(nodesTable), pq.QuoteIdentifier(nodesTable)))
	if err != nil {
		return err
	}

	// Create indexes on the edges table with prefix
	_, err = g.Pool.Exec(ctx, fmt.Sprintf(`
		 CREATE INDEX IF NOT EXISTS idx_%s_source_node_id ON %s (source_node_id);
		 CREATE INDEX IF NOT EXISTS idx_%s_target_node_id ON %s (target_node_id);
	 `, g.Name, pq.QuoteIdentifier(edgesTable), g.Name, pq.QuoteIdentifier(edgesTable)))
	if err != nil {
		return err
	}

	return nil
}

// InsertNode inserts a new node into the database.
func (g *GorphPG) InsertNode(node *Node) error {
	ctx := context.Background()

	// Convert the Value field to JSON
	valueJSON, err := json.Marshal(node.Value)
	if err != nil {
		return err
	}

	// Create the nodes table name with the graph name as a prefix
	nodesTable := fmt.Sprintf("%s_nodes", g.Name)

	// Prepare SQL statement for inserting the node
	sql := fmt.Sprintf(`INSERT INTO %s (node_id, node_type, node_value) VALUES ($1, $2, $3);`, pq.QuoteIdentifier(nodesTable))

	// Execute the query
	_, err = g.Pool.Exec(ctx, sql, node.ID, node.Type, valueJSON)
	if err != nil {
		return err
	}

	return nil
}

// InsertEdge inserts a new edge into the database.
func (g *GorphPG) InsertEdge(edge *Edge) error {
	ctx := context.Background()

	// Create the edges table name with the graph name as a prefix
	edgesTable := fmt.Sprintf("%s_edges", g.Name)

	sql := fmt.Sprintf(`INSERT INTO %s (edge_id, source_node_id, target_node_id, edge_type) VALUES ($1, $2, $3, $4);`, pq.QuoteIdentifier(edgesTable))

	_, err := g.Pool.Exec(ctx, sql, edge.ID, edge.Source, edge.Target, edge.Type)
	return err
}

// QueryBuilder builds a SQL query for nodes.
type QueryBuilder struct {
	GraphName  string // Name of the Graph
	conditions []string
	joins      []string
}

// NewQueryBuilder creates a new instance of QueryBuilder with the graph name.
func (g *GorphPG) NewQueryBuilder() *QueryBuilder {
	return &QueryBuilder{
		GraphName: g.Name, // Set the graph name in QueryBuilder
	}
}

// Where adds a condition to the query.
func (b *QueryBuilder) Where(condition string) *QueryBuilder {
	b.conditions = append(b.conditions, condition)
	return b
}

// SourceOf adds a JOIN condition to the query to find source nodes of a given target node.
func (b *QueryBuilder) SourceOf(targetID uuid.UUID) *QueryBuilder {
	alias := fmt.Sprintf("e%d", len(b.joins))          // Unique alias for each join
	edgesTable := fmt.Sprintf("%s_edges", b.GraphName) // Prefixing edges table with graph name
	joinCondition := fmt.Sprintf("JOIN %s %s ON %s.target_node_id = nodes.node_id AND %s.source_node_id = '%s'", pq.QuoteIdentifier(edgesTable), alias, alias, alias, targetID)
	b.joins = append(b.joins, joinCondition)
	return b
}

// TargetOf adds a JOIN condition to the query to find target nodes of a given source node.
func (b *QueryBuilder) TargetOf(sourceID uuid.UUID) *QueryBuilder {
	alias := fmt.Sprintf("e%d", len(b.joins))          // Unique alias for each join
	edgesTable := fmt.Sprintf("%s_edges", b.GraphName) // Prefixing edges table with graph name
	joinCondition := fmt.Sprintf("JOIN %s %s ON %s.target_node_id = '%s' AND %s.source_node_id = nodes.node_id", pq.QuoteIdentifier(edgesTable), alias, alias, sourceID, alias)
	b.joins = append(b.joins, joinCondition)
	return b
}

// EdgeType adds a JOIN condition to the query to find edges of a specific type.
func (b *QueryBuilder) EdgeType(edgeType string) *QueryBuilder {
	alias := fmt.Sprintf("e%d", len(b.joins))          // Unique alias for each join
	edgesTable := fmt.Sprintf("%s_edges", b.GraphName) // Prefixing edges table with graph name
	joinCondition := fmt.Sprintf("JOIN %s %s ON (%s.source_node_id = nodes.node_id OR %s.target_node_id = nodes.node_id) AND %s.edge_type = '%s'", pq.QuoteIdentifier(edgesTable), alias, alias, alias, alias, edgeType)
	b.joins = append(b.joins, joinCondition)
	return b
}

// ToSQL generates the SQL query as a string.
func (b *QueryBuilder) ToSQL() string {
	// Include the graph name in the nodes table name
	nodesTable := fmt.Sprintf("%s_nodes", b.GraphName)

	// Construct the base query with the prefixed nodes table
	baseQuery := fmt.Sprintf("SELECT %s.node_id, %s.node_type, %s.node_value FROM %s ", pq.QuoteIdentifier(nodesTable), pq.QuoteIdentifier(nodesTable), pq.QuoteIdentifier(nodesTable), pq.QuoteIdentifier(nodesTable))

	joinClause := ""
	if len(b.joins) > 0 {
		joinClause = strings.Join(b.joins, " ")
	}

	whereClause := ""
	if len(b.conditions) > 0 {
		whereClause = fmt.Sprintf("WHERE %s", strings.Join(b.conditions, " AND "))
	}

	return fmt.Sprintf("%s%s %s;", baseQuery, joinClause, whereClause)
}

// Execute runs the query built by QueryBuilder and returns the results.
func (g *GorphPG) Execute(builder *QueryBuilder) ([]Node, error) {
	ctx := context.Background()
	sql := builder.ToSQL()

	g.Logger.Info().
		Fields(map[string]interface{}{
			"SQL": sql,
		}).Msg("executing query")
	rows, err := g.Pool.Query(ctx, sql)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var nodes []Node
	for rows.Next() {
		var node Node
		var valueJSON []byte
		if err := rows.Scan(&node.ID, &node.Type, &valueJSON); err != nil {
			return nil, err
		}
		// For simplicity, assuming Value is a raw JSON string
		node.Value = string(valueJSON)
		nodes = append(nodes, node)
	}

	return nodes, nil
}
